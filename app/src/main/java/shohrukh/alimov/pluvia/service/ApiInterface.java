package shohrukh.alimov.pluvia.service;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import shohrukh.alimov.pluvia.model.current.CurrentResponse;
import shohrukh.alimov.pluvia.model.flickr.FlickrResponse;
import shohrukh.alimov.pluvia.model.daily.DailyForecastResponse;

/**
 * Created by alim on 1/31/18.
 */

public interface ApiInterface {

    @GET("weather")
    Call<CurrentResponse> getCurrent(@Query("id") int cityId, @Query("appid") String appId, @Query("units") String units, @Query("lang") String lang);

    @GET("forecast")
    Call<DailyForecastResponse> getForecast(@Query("id") int cityId, @Query("appid") String appId, @Query("units") String units, @Query("lang") String lang);

    @GET("?method=flickr.photos.search" +
            "&api_key=45fbcbab650bdf2c81bb27adf3ebd279" +
            "&sort=relevance" +
            "&accuracy=11" +
            "&content_type=1" +
            "&extras=url_c" +
            "&format=json" +
            "&nojsoncallback=1")
    Call<FlickrResponse> getFlickrMeta(@Query("text") String text,
                                       @Query("tag") String tag);
}
