package shohrukh.alimov.pluvia.service;

import android.app.Activity;
import android.widget.Toast;

import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import shohrukh.alimov.pluvia.R;

/**
 * Created by alim on 1/31/18.
 */

public class ApiClient {

    /**
     * @return Retrofit
     * .client(new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build())
     */
    public static ApiInterface getRetrofit(String baseURL) {
        return new Retrofit.Builder().baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build())
                .build().create(ApiInterface.class);
    }

    /**
     * @param activity Context
     * @param response Response error in case something went wrong
     */
    public static void responseError(Activity activity, Response response) {
        try {
            JSONObject json = new JSONObject(response.errorBody().string());
            if (json != null)
                Toast.makeText(activity, json.optString("message"), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(activity, activity.getString(R.string.somethingWentWrong), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
