package shohrukh.alimov.pluvia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import shohrukh.alimov.pluvia.R;
import shohrukh.alimov.pluvia.model.daily.DailyMain;
import shohrukh.alimov.pluvia.model.daily.DailyWeather;
import shohrukh.alimov.pluvia.model.daily.DailyList;

import static shohrukh.alimov.pluvia.configs.Methods.getDate;
import static shohrukh.alimov.pluvia.configs.Methods.getDrawable;
import static shohrukh.alimov.pluvia.configs.Methods.roundUp;

/**
 * Created by alim on 1/5/18.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    private List<DailyMain> mainList;
    private HashMap<Integer, List<DailyWeather>> weatherList;
    private List<DailyList> dailylistModel;
    private List<Long> longList;
    private LayoutInflater mInflater;
    private Context context;

    public ForecastAdapter(Context context, List<Long> longList,
                           List<DailyList> dailylistModel, List<DailyMain> mainModel, HashMap<Integer, List<DailyWeather>> weatherList) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.mainList = mainModel;
        this.dailylistModel = dailylistModel;
        this.longList = longList;
        this.weatherList = weatherList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_forecast, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        long valList = longList.get(position);
        DailyMain model = mainList.get(position);

        List<DailyWeather> weathers = weatherList.get(position);
        if (weathers != null) {
            for (DailyWeather w : weathers) {
                StringBuilder resName = new StringBuilder(w.getIcon());
                resName.insert(0, "_");

                Glide.with(context).load(null)
                        .apply(new RequestOptions().fallback(getDrawable(context,
                                resName.toString()))).into(holder.conditionImage);

            }
        }
        holder.forecastDayName.setText(getDate(valList));


        holder.forecastHigh.setText(context.getString(R.string.degrees, roundUp(model.getTemp_max())));
        holder.forecastLow.setText(context.getString(R.string.degrees, roundUp(model.getTemp_min())));
    }

    @Override
    public int getItemCount() {
        return longList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.forecastDayName)
        TextView forecastDayName;
        @BindView(R.id.forecastHigh)
        TextView forecastHigh;
        @BindView(R.id.forecastLow)
        TextView forecastLow;
        @BindView(R.id.conditionImage)
        ImageView conditionImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}