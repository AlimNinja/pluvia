package shohrukh.alimov.pluvia.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import shohrukh.alimov.pluvia.R;
import shohrukh.alimov.pluvia.configs.UserPrefs;
import shohrukh.alimov.pluvia.fragment.FragmentMain;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static shohrukh.alimov.pluvia.MyApplication.context;

public class ActivityMain extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.navigationView)
    NavigationView navigationView;
    ActionBarDrawerToggle toggle;
    View header;
    static TextView cityName;

    boolean exit = false;
    static int size;
    UserPrefs userPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        userPrefs = new UserPrefs(this);
        cityName = findViewById(R.id.cityName);

        initDrawer(toolbar);
        initialize();
        loadFragment(new FragmentMain());
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        if (toggle != null) toggle.syncState();
    }

    private void initDrawer(Toolbar toolbar) {
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);
        drawerLayout.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
    }

    private void initialize() {
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);
    }

    public static void setToolbarTitle(String value) {
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            size = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        llp.setMargins(0, 0, size, 0);

        cityName.setText(value);
        cityName.setGravity(Gravity.CENTER);
        cityName.setLayoutParams(llp);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int cityInt = 0;
        switch (menuItem.getItemId()) {
            case R.id.action1:
                cityInt = 2643123;
                break;
            case R.id.action2:
                cityInt = 524901;
                break;
            case R.id.action3:
                cityInt = 1733046;
                break;
            case R.id.action4:
                cityInt = 1880251;
                break;
            case R.id.action5:
                cityInt = 6359304;
                break;
            case R.id.action6:
                cityInt = 498817;
                break;
            case R.id.action7:
                cityInt = 1526384;
                break;
            case R.id.action8:
                cityInt = 1816670;
                break;
            case R.id.action9:
                cityInt = 2988507;
                break;
            case R.id.action10:
                cityInt = 1796236;
                break;
            case R.id.action11:
                cityInt = 3451190;
                break;
            case R.id.action12:
                cityInt = 3996063;
                break;
            case R.id.action13:
                cityInt = 5380748;
                break;
            case R.id.action14:
                cityInt = 5128581;
                break;
            case R.id.action15:
                cityInt = 5506956;
                break;
            case R.id.action16:
                cityInt = 5391959;
                break;
            case R.id.action17:
                cityInt = 4183849;
                break;
            case R.id.action18:
                cityInt = 4839366;
                break;
            case R.id.action19:
                cityInt = 1850147;
                break;
            case R.id.action20:
                cityInt = 3067696;
                break;
        }

        userPrefs.saveWeatherCity(cityInt);
        setToolbarTitle(menuItem.toString());
        userPrefs.saveFlickrMeta(menuItem.getTitle().toString(), menuItem.getTitle().toString());
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();
        loadFragment(new FragmentMain());
        return true;
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit)
                .replace(R.id.contentBaseDrawer, fragment)
                .addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.contentBaseDrawer);
            if (fragment instanceof FragmentMain) {
                if (exit) {
                    moveTaskToBack(true);
                } else {
                    Toast.makeText(this, getString(R.string.backAgain), Toast.LENGTH_SHORT).show();
                    exit = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            exit = false;
                        }
                    }, 2000);
                }
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
