package shohrukh.alimov.pluvia.fragment;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.blurry.Blurry;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shohrukh.alimov.pluvia.R;
import shohrukh.alimov.pluvia.adapter.ForecastAdapter;
import shohrukh.alimov.pluvia.configs.UserPrefs;
import shohrukh.alimov.pluvia.model.current.CurrentMain;
import shohrukh.alimov.pluvia.model.current.CurrentResponse;
import shohrukh.alimov.pluvia.model.current.CurrentWeather;
import shohrukh.alimov.pluvia.model.daily.DailyForecastResponse;
import shohrukh.alimov.pluvia.model.daily.DailyList;
import shohrukh.alimov.pluvia.model.daily.DailyMain;
import shohrukh.alimov.pluvia.model.daily.DailyWeather;
import shohrukh.alimov.pluvia.model.flickr.FlickrResponse;
import shohrukh.alimov.pluvia.model.flickr.ModelPhoto;
import shohrukh.alimov.pluvia.model.flickr.Photos;
import shohrukh.alimov.pluvia.service.ApiClient;

import static shohrukh.alimov.pluvia.MyApplication.context;
import static shohrukh.alimov.pluvia.activity.ActivityMain.setToolbarTitle;
import static shohrukh.alimov.pluvia.configs.Constants.defaultCityId;
import static shohrukh.alimov.pluvia.configs.Constants.defaultFlickrTag;
import static shohrukh.alimov.pluvia.configs.Constants.defaultFlickrText;
import static shohrukh.alimov.pluvia.configs.Constants.defaultLang;
import static shohrukh.alimov.pluvia.configs.Constants.defaultUnits;
import static shohrukh.alimov.pluvia.configs.EndPoint.FLICKR_URL;
import static shohrukh.alimov.pluvia.configs.EndPoint.WEATHER_URL;
import static shohrukh.alimov.pluvia.configs.Methods.getColors;
import static shohrukh.alimov.pluvia.configs.Methods.getDrawable;
import static shohrukh.alimov.pluvia.configs.Methods.roundUp;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMain extends Fragment implements Callback<FlickrResponse> {

    @BindView(R.id.fragmentContent)
    FrameLayout fragmentContent;
    @BindView(R.id.backy)
    ImageView backy;
    @BindView(R.id.mainView)
    LinearLayout mainView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;
    @BindView(R.id.conditionImage)
    ImageView conditionImage;
    @BindView(R.id.conditionName)
    TextView conditionName;
    @BindView(R.id.currentTemp)
    TextView currentTemp;
    @BindView(R.id.highTemp)
    TextView highTemp;
    @BindView(R.id.lowTemp)
    TextView lowTemp;
    @BindView(R.id.dailyForecastRecycler)
    RecyclerView forecastRecycler;
    @BindView(R.id.refreshView)
    Button refreshView;
    @BindView(R.id.celsiusUnit)
    TextView celsiusUnit;
    @BindView(R.id.fahrenheitUnit)
    TextView fahrenheitUnit;

    BottomSheetBehavior sheetBehavior;
    ForecastAdapter fAdapter;
    UserPrefs userPrefs;

    private List<DailyList> dailyListModel;
    private List<Long> dtList = new ArrayList<>();
    private List<DailyMain> dailyMains = new ArrayList<>();
    private HashMap<Integer, List<DailyWeather>> dailyWeatherIcon = new HashMap<>();

    String flickrText;
    String flickrTag;
    int cityId;
    String units;
    String lang;

    public FragmentMain() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        userPrefs = new UserPrefs(getActivity());
        getConstants();
        bottomSheet();

        /*  First API Call for Flickr Image (city as background) */
        Call<FlickrResponse> call = ApiClient.getRetrofit(FLICKR_URL).getFlickrMeta(flickrText, flickrTag);
        call.enqueue(this);
    }

    /**
     * Get settings accordingly (if null, gets default constant values)
     */
    private void getConstants() {
        flickrText = userPrefs.getUserPrefs().getString("flickrText", defaultFlickrText);
        flickrTag = userPrefs.getUserPrefs().getString("flickrTag", defaultFlickrTag);
        cityId = userPrefs.getUserPrefs().getInt("cityId", defaultCityId);
        units = userPrefs.getUserPrefs().getString("units", defaultUnits);
        lang = userPrefs.getUserPrefs().getString("lang", defaultLang);

        if (units.equals("metric")) {
            celsiusUnit.setTextColor(getColors(R.color.colorTextWhite));
            fahrenheitUnit.setTextColor(getColors(R.color.colorTextGrey));
        } else {
            celsiusUnit.setTextColor(getColors(R.color.colorTextGrey));
            fahrenheitUnit.setTextColor(getColors(R.color.colorTextWhite));
        }
    }

    /**
     * Initializes BottomSheetBehavior
     */
    private void bottomSheet() {
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        Blurry.with(context)
                                .radius(5)
                                .sampling(5)
                                .async()
                                .animate()
                                .onto(fragmentContent);
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        Blurry.delete(fragmentContent);
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        forecastRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

    /**
     * Action to open 5 day forecast view
     */
    @OnClick(R.id.backy)
    void openForecastView() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    /**
     * Action to open or close 5 day forecast view
     */
    @OnClick(R.id.bottom_sheet)
    void sheetState() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    /**
     * Action to refresh view and call for the First API call (in case Flickr image loading failed)
     */
    @OnClick(R.id.refreshView)
    void RefreshView() {
        refreshView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        Call<FlickrResponse> call = ApiClient.getRetrofit(FLICKR_URL).getFlickrMeta(flickrText, flickrTag);
        retryFlickr(call);
        if (dailyListModel != null) {
            dailyListModel.clear();
            dtList.clear();
            dailyWeatherIcon.clear();
            fAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Change unit to celsius
     */
    @OnClick(R.id.celsiusUnit)
    void changeToCelsius() {
        if (!units.equals("metric")) {
            userPrefs.saveWeatherUnits("metric");
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    /**
     * Change unit to fahrenheit
     */
    @OnClick(R.id.fahrenheitUnit)
    void changeToFahrenheit() {
        if (!units.equals("imperial")) {
            userPrefs.saveWeatherUnits("imperial");
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    /**
     * Get current Weather
     */
    private void getCurrent() {
        Call<CurrentResponse> call = ApiClient.getRetrofit(WEATHER_URL).getCurrent(cityId,
                getString(R.string.open_weather_key), units, lang);
        call.enqueue(new Callback<CurrentResponse>() {
            @Override
            public void onResponse(Call<CurrentResponse> call, Response<CurrentResponse> response) {
                if (response.code() == 200) {
                    CurrentMain main = response.body().getMain();
                    currentTemp.setText(getString(R.string.degrees, roundUp(main.getTemp())));
                    highTemp.setText(getString(R.string.degrees, roundUp(main.getTemp_max())));
                    lowTemp.setText(getString(R.string.degrees, roundUp(main.getTemp_min())));

                    List<CurrentWeather> weatherList = response.body().getWeather();
                    for (CurrentWeather weather : weatherList) {
                        StringBuilder resName = new StringBuilder(weather.getIcon());
                        resName.insert(0, "_");
                        Glide.with(context).load(null)
                                .apply(new RequestOptions().fallback(getDrawable(getActivity(),
                                        resName.toString()))).into(conditionImage);

                        String conditionStr = weather.getDescription().substring(0, 1).toUpperCase() + weather.getDescription().substring(1);
                        conditionName.setText(conditionStr);
                    }
                    setToolbarTitle(response.body().getName());
                    getForecast();
                } else {
                    ApiClient.responseError(getActivity(), response);
                }
            }

            @Override
            public void onFailure(Call<CurrentResponse> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), getString(R.string.lowInternet), Toast.LENGTH_LONG).show();
                } else if (t instanceof ConnectException) {
                    Toast.makeText(getActivity(), getString(R.string.checkConnection), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.somethingWentWrong), Toast.LENGTH_SHORT).show();
                    Log.e("Error Response", t.getMessage());
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    /**
     * Get 5 day forecast
     */
    private void getForecast() {
        Call<DailyForecastResponse> call = ApiClient.getRetrofit(WEATHER_URL).getForecast(cityId,
                getString(R.string.open_weather_key), units, lang);
        call.enqueue(new Callback<DailyForecastResponse>() {
            @Override
            public void onResponse(Call<DailyForecastResponse> call, Response<DailyForecastResponse> response) {
                dailyListModel = response.body().getList();

                int loop = 0;
                for (int i = 0; i < dailyListModel.size(); i += 8) {
                    dailyMains.add(loop, dailyListModel.get(i).getMain());
                    dtList.add(loop, dailyListModel.get(i).getDt());
                    dailyWeatherIcon.put(loop, dailyListModel.get(i).getWeather());
                    loop++;
                }
                fAdapter = new ForecastAdapter(getActivity(), dtList, dailyListModel, dailyMains, dailyWeatherIcon);
                forecastRecycler.setAdapter(fAdapter);
                fAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<DailyForecastResponse> call, Throwable t) {

            }
        });
    }

    /**
     * @param call     Flickr call response vai Retrofit
     * @param response Call response was 200
     */
    @Override
    public void onResponse(Call<FlickrResponse> call, Response<FlickrResponse> response) {
        Photos photos = response.body().getPhotos();
        List<ModelPhoto> modelList = photos.getPhoto();
        ModelPhoto photo = modelList.get(new Random().nextInt(modelList.size()));
        mainView.setVisibility(View.VISIBLE);

        Glide.with(getActivity()).load(photo.getUrl_c())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        refreshView.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.errorInFlickr), Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(backy);
        getCurrent();
    }

    /**
     * @param call Flickr call response vai Retrofit
     * @param t    Call response was not 200
     */
    @Override
    public void onFailure(Call<FlickrResponse> call, Throwable t) {
        if (t instanceof SocketTimeoutException) {
            Toast.makeText(getActivity(), getString(R.string.lowInternet), Toast.LENGTH_LONG).show();
            retryFlickr(call);
        } else if (t instanceof ConnectException) {
            Toast.makeText(getActivity(), getString(R.string.checkConnection), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
        } else {
            Toast.makeText(getActivity(), getString(R.string.somethingWentWrong), Toast.LENGTH_SHORT).show();
            Log.e("Error Response", t.getMessage());
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * @param call Retry Flickr call in case of fail
     */
    private void retryFlickr(Call<FlickrResponse> call) {
        call.clone().enqueue(this);
    }
}
