package shohrukh.alimov.pluvia.model.daily;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DailyList {
    @Expose
    @SerializedName("dt_txt")
    private String dt_txt;
    @Expose
    @SerializedName("sys")
    private DailySys sys;
    @Expose
    @SerializedName("snow")
    private DailySnow snow;
    @Expose
    @SerializedName("wind")
    private DailyWind wind;
    @Expose
    @SerializedName("clouds")
    private DailyClouds clouds;
    @Expose
    @SerializedName("weather")
    private java.util.List<DailyWeather> weather;
    @Expose
    @SerializedName("main")
    private DailyMain main;
    @Expose
    @SerializedName("dt")
    private long dt;

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public DailySys getSys() {
        return sys;
    }

    public void setSys(DailySys sys) {
        this.sys = sys;
    }

    public DailySnow getSnow() {
        return snow;
    }

    public void setSnow(DailySnow snow) {
        this.snow = snow;
    }

    public DailyWind getWind() {
        return wind;
    }

    public void setWind(DailyWind wind) {
        this.wind = wind;
    }

    public DailyClouds getClouds() {
        return clouds;
    }

    public void setClouds(DailyClouds clouds) {
        this.clouds = clouds;
    }

    public List<DailyWeather> getWeather() {
        return weather;
    }

    public void setWeather(List<DailyWeather> weather) {
        this.weather = weather;
    }

    public DailyMain getMain() {
        return main;
    }

    public void setMain(DailyMain main) {
        this.main = main;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }
}
