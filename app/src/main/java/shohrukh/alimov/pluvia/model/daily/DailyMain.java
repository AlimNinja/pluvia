package shohrukh.alimov.pluvia.model.daily;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyMain {
    @Expose
    @SerializedName("temp_kf")
    private double temp_kf;
    @Expose
    @SerializedName("humidity")
    private int humidity;
    @Expose
    @SerializedName("grnd_level")
    private double grnd_level;
    @Expose
    @SerializedName("sea_level")
    private double sea_level;
    @Expose
    @SerializedName("pressure")
    private double pressure;
    @Expose
    @SerializedName("temp_max")
    private String temp_max;
    @Expose
    @SerializedName("temp_min")
    private String temp_min;
    @Expose
    @SerializedName("temp")
    private String temp;

    public double getTemp_kf() {
        return temp_kf;
    }

    public void setTemp_kf(double temp_kf) {
        this.temp_kf = temp_kf;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public double getGrnd_level() {
        return grnd_level;
    }

    public void setGrnd_level(double grnd_level) {
        this.grnd_level = grnd_level;
    }

    public double getSea_level() {
        return sea_level;
    }

    public void setSea_level(double sea_level) {
        this.sea_level = sea_level;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public String getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(String temp_max) {
        this.temp_max = temp_max;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(String temp_min) {
        this.temp_min = temp_min;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
