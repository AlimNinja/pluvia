package shohrukh.alimov.pluvia.model.current;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alim on 1/31/18.
 */

public class CurrentResponse {

    @Expose
    @SerializedName("cod")
    private int cod;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("sys")
    private CurrentSys sys;
    @Expose
    @SerializedName("dt")
    private int dt;
    @Expose
    @SerializedName("clouds")
    private CurrentClouds clouds;
    @Expose
    @SerializedName("wind")
    private CurrentWind wind;
    @Expose
    @SerializedName("visibility")
    private int visibility;
    @Expose
    @SerializedName("main")
    private CurrentMain main;
    @Expose
    @SerializedName("base")
    private String base;
    @Expose
    @SerializedName("weather")
    private List<CurrentWeather> weather;
    @Expose
    @SerializedName("coord")
    private CurrentCoord coord;

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CurrentSys getSys() {
        return sys;
    }

    public void setSys(CurrentSys sys) {
        this.sys = sys;
    }

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public CurrentClouds getClouds() {
        return clouds;
    }

    public void setClouds(CurrentClouds clouds) {
        this.clouds = clouds;
    }

    public CurrentWind getWind() {
        return wind;
    }

    public void setWind(CurrentWind wind) {
        this.wind = wind;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public CurrentMain getMain() {
        return main;
    }

    public void setMain(CurrentMain main) {
        this.main = main;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public List<CurrentWeather> getWeather() {
        return weather;
    }

    public void setWeather(List<CurrentWeather> weather) {
        this.weather = weather;
    }

    public CurrentCoord getCoord() {
        return coord;
    }

    public void setCoord(CurrentCoord coord) {
        this.coord = coord;
    }
}
