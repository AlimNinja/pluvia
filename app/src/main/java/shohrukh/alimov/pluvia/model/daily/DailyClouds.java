package shohrukh.alimov.pluvia.model.daily;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyClouds {
    @Expose
    @SerializedName("all")
    private int all;

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
