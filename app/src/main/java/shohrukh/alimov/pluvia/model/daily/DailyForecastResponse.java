package shohrukh.alimov.pluvia.model.daily;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alim on 1/31/18.
 */

public class DailyForecastResponse {

    @Expose
    @SerializedName("city")
    private DailyCity city;
    @Expose
    @SerializedName("list")
    private List<DailyList> list;
    @Expose
    @SerializedName("cnt")
    private int cnt;
    @Expose
    @SerializedName("message")
    private double message;
    @Expose
    @SerializedName("cod")
    private String cod;

    public DailyCity getCity() {
        return city;
    }

    public void setCity(DailyCity city) {
        this.city = city;
    }

    public List<DailyList> getList() {
        return list;
    }

    public void setList(List<DailyList> list) {
        this.list = list;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }
}
