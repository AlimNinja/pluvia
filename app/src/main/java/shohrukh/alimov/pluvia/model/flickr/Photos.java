package shohrukh.alimov.pluvia.model.flickr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Photos {
    @Expose
    @SerializedName("photo")
    private List<ModelPhoto> photo;
    @Expose
    @SerializedName("total")
    private String total;
    @Expose
    @SerializedName("perpage")
    private int perpage;
    @Expose
    @SerializedName("pages")
    private int pages;
    @Expose
    @SerializedName("page")
    private int page;

    public List<ModelPhoto> getPhoto() {
        return photo;
    }

    public void setPhoto(List<ModelPhoto> photo) {
        this.photo = photo;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public int getPerpage() {
        return perpage;
    }

    public void setPerpage(int perpage) {
        this.perpage = perpage;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
