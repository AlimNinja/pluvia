package shohrukh.alimov.pluvia.model.daily;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyCity {
    @Expose
    @SerializedName("country")
    private String country;
    @Expose
    @SerializedName("coord")
    private DailyCoord coord;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("id")
    private int id;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public DailyCoord getCoord() {
        return coord;
    }

    public void setCoord(DailyCoord coord) {
        this.coord = coord;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
