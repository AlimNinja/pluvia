package shohrukh.alimov.pluvia.model.flickr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by alim on 2/2/18.
 */

public class FlickrResponse {

    @Expose
    @SerializedName("stat")
    private String stat;
    @Expose
    @SerializedName("photos")
    private Photos photos;

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }
}
