package shohrukh.alimov.pluvia.model.current;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentMain {
    @Expose
    @SerializedName("temp_max")
    private String temp_max;
    @Expose
    @SerializedName("temp_min")
    private String temp_min;
    @Expose
    @SerializedName("humidity")
    private String humidity;
    @Expose
    @SerializedName("pressure")
    private String pressure;
    @Expose
    @SerializedName("temp")
    private String temp;

    public String getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(String temp_max) {
        this.temp_max = temp_max;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(String temp_min) {
        this.temp_min = temp_min;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
