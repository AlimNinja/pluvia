package shohrukh.alimov.pluvia.model.flickr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelPhoto {

    @Expose
    @SerializedName("width_c")
    private String width_c;
    @Expose
    @SerializedName("height_c")
    private int height_c;
    @Expose
    @SerializedName("url_c")
    private String url_c;
    @Expose
    @SerializedName("isfamily")
    private int isfamily;
    @Expose
    @SerializedName("isfriend")
    private int isfriend;
    @Expose
    @SerializedName("ispublic")
    private int ispublic;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("farm")
    private int farm;
    @Expose
    @SerializedName("server")
    private String server;
    @Expose
    @SerializedName("secret")
    private String secret;
    @Expose
    @SerializedName("owner")
    private String owner;
    @Expose
    @SerializedName("id")
    private String id;

    public String getWidth_c() {
        return width_c;
    }

    public void setWidth_c(String width_c) {
        this.width_c = width_c;
    }

    public int getHeight_c() {
        return height_c;
    }

    public void setHeight_c(int height_c) {
        this.height_c = height_c;
    }

    public String getUrl_c() {
        return url_c;
    }

    public void setUrl_c(String url_c) {
        this.url_c = url_c;
    }

    public int getIsfamily() {
        return isfamily;
    }

    public void setIsfamily(int isfamily) {
        this.isfamily = isfamily;
    }

    public int getIsfriend() {
        return isfriend;
    }

    public void setIsfriend(int isfriend) {
        this.isfriend = isfriend;
    }

    public int getIspublic() {
        return ispublic;
    }

    public void setIspublic(int ispublic) {
        this.ispublic = ispublic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFarm() {
        return farm;
    }

    public void setFarm(int farm) {
        this.farm = farm;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
