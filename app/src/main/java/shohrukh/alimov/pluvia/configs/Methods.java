package shohrukh.alimov.pluvia.configs;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.view.MenuItem;

import java.util.Calendar;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;

import static shohrukh.alimov.pluvia.MyApplication.context;

/**
 * Created by alim on 12/29/17.
 */

public class Methods {

    /**
     * Apply custom font to Menu
     *
     * @param mi       Menu
     * @param activity Context
     */
    public static void applyFontToMenu(MenuItem mi, Activity activity) {
        Typeface font = Typeface.createFromAsset(activity.getAssets(), "fonts/Regular.ttf");
        SpannableString fontableTitle = new SpannableString(mi.getTitle());
        fontableTitle.setSpan(new CalligraphyTypefaceSpan(font), 0, fontableTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(fontableTitle);
    }

    /**
     * @param id Color name
     */
    public static int getColors(int id) {
        return ContextCompat.getColor(context, id);
    }

    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("EEEE", cal).toString();
        return date;
    }

    /**
     * @param temp Temperature
     * @return Rounded temp
     */
    public static String roundUp(String temp) {
        String newTemp = temp;
        if (temp.contains(".")) {
            double number = Double.parseDouble(newTemp);
            newTemp = String.valueOf(Math.round(number));
        }
        return newTemp;
    }

    /**
     * @param context   Activity
     * @param ImageName CurrentWeather Icon name
     * @return Icon from Drawable
     */
    public static Drawable getDrawable(Context context, String ImageName) {
        return ContextCompat.getDrawable(context, context.getResources().getIdentifier(ImageName, "drawable", context.getPackageName()));
    }
}