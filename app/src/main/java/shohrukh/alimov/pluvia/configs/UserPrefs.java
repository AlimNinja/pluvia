package shohrukh.alimov.pluvia.configs;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static shohrukh.alimov.pluvia.MyApplication.context;

/**
 * Created by alim on 2/4/18.
 */

public class UserPrefs {

    public static SharedPreferences userPref;
    public static SharedPreferences.Editor editor;
    public Context _context;

    public static final String PREF_NAME = "userSettings";

    public UserPrefs(Context context) {
        this._context = context;
        userPref = _context.getSharedPreferences(PREF_NAME, 0);
    }

    public void saveFlickrMeta(String flickrText, String flickrTag) {
        editor = userPref.edit();
        editor.putString("flickrText", flickrText);
        editor.putString("flickrTag", flickrTag);
        editor.apply();
    }

    public void saveWeatherCity(int cityId) {
        editor = userPref.edit();
        editor.putInt("cityId", cityId);
        editor.apply();
    }

    public void saveWeatherUnits(String units) {
        editor = userPref.edit();
        editor.putString("units", units);
        editor.apply();
    }

    public void saveWeatherLang(String lang) {
        editor = userPref.edit();
        editor.putString("lang", lang);
        editor.apply();
    }


    public SharedPreferences getUserPrefs() {
        return context.getSharedPreferences(UserPrefs.PREF_NAME, MODE_PRIVATE);
    }
}
