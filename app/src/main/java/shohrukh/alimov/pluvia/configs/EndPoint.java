package shohrukh.alimov.pluvia.configs;

/**
 * Created by alim on 2/2/18.
 */

public class EndPoint {

    public static final String FLICKR_URL = "https://api.flickr.com/services/rest/";
    public static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/";
}
