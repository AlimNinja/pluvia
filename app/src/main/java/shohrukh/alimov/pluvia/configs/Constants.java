package shohrukh.alimov.pluvia.configs;

/**
 * Created by alim on 2/4/18.
 */

public class Constants {

    public static String defaultFlickrText = "Moscow";
    public static String defaultFlickrTag = "Moscow";
    public static int defaultCityId = 524901;
    public static String defaultUnits = "metric";
    public static String defaultLang = "en";

}
